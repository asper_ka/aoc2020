text = """nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6"""

text = open("input.txt").read()


orig_code = text.split ("\n")
if orig_code[-1] == "":
    orig_code = orig_code[:-1]

code = []

acc = 0
ip_visited = set()

def execute_next_line(ip, acc):
    #print (ip, code[ip], len(code))
    (cmd, value) = tuple(code[ip].split())
    if (cmd == "acc"):
        acc += int(value)
    elif (cmd == "jmp"):
        return ip + int(value), acc
    return ip+1, acc

def run_program():
    acc = 0
    ip = 0
    ip_visited = set()
    while not ip in ip_visited:
        ip_visited.add(ip)
        (ip, acc) = execute_next_line(ip, acc)
        if (ip == len(code)):
            return (True, acc)
    return (False, acc)

for change_line in range(len(orig_code)):
    code = []
    for l in range(len(orig_code)):
        cmd = orig_code[l]
        if (change_line==l):
            cmd = cmd.replace("jmp", "nop")
        code.append(cmd)
    (result, acc) = run_program()
    if result:
        print (acc, change_line)
    code = []
    for l in range(len(orig_code)):
        cmd = orig_code[l]
        if (change_line==l):
            cmd = cmd.replace("nop", "jmp")
        code.append(cmd)
    (result, acc) = run_program()
    if result:
        print (acc, change_line)

