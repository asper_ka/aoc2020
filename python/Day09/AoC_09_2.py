text = """35
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576
"""
target = 127

text = open("input.txt").read()
target = 400480901


lines = text.split('\n')

numbers = [int(l) for l in lines if l != ""]

for start in range (len(numbers)-1):
    sum = 0
    end = start
    while (sum < target):
        sum += numbers[end]
        if (sum == target):
            print (start, end, sum)
            nrs = numbers[start:end+1]
            print (nrs)
            print (max(nrs), min(nrs), max(nrs)+min(nrs))
        end += 1