import itertools

text = open("input.txt").read()

lines = text.split("\n")
while lines[-1] == "":
    lines = lines[:-1]

print (len(lines))
nrs = [int(x) for x in lines]

print (nrs.index(800))

while len(nrs) > 1010:
    print ("*******************************************************")
    idcs = itertools.combinations(range(0, len(nrs)), 1010)
    for i in idcs:
        sum = 0
        #print (i)
        for k in range(1010):
            sum += nrs[i[k]]
        #print (sum)
        if sum == 4080400:
            print (sum)

