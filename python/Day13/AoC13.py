text="""939
7,13,x,x,59,x,31,19"""

text = open("input.txt").read()

lines = text.split('\n')

time = int(lines[0])
ids = [int(x) for x in lines[1].split(',') if x!='x']
print (time, ids)

diffs = {}
for id in ids:
    diffs[id] = id-(time%id)

minkey = min(diffs, key=diffs.get)

print (minkey, diffs[minkey], diffs)
print (minkey * diffs[minkey])