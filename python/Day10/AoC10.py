text = """16
10
15
5
1
11
7
19
6
12
4"""

text="""28
33
18
42
31
14
46
20
48
47
24
23
49
45
19
38
39
11
1
32
25
35
8
17
7
9
4
2
34
10
3"""

text = open("input.txt").read()

lines = text.split('\n')
numbers = [int(l) for l in lines if l != ""]
numbers.append(0)
numbers.append(max(numbers)+3)

diff1 = 0
diff3 = 0
numbers.sort()
print (numbers)
for i in range (len(numbers)-1):
    diff = numbers[i+1] - numbers[i]
    if (diff == 1):
        diff1 += 1
    elif (diff == 3):
        diff3 += 1

print (diff1, diff3, diff1*diff3)