# -*- coding: utf-8 -*-


import re

text = """abc

a
b
c

ab
ac

a
a
a
a

b"""

text = open("input.txt").read()

lines = text.split("\n")

groups = []
chars = set()
for l in lines:
    if len(l)==0:
        groups.append(chars)
        chars = set()
    else:
        for c in l:
            chars.add(c)

groups.append(chars)
sum = 0
for g in groups:
    print (g)
    sum += len(g)

print (sum)

