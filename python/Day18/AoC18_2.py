import numpy

text = """1 + 2 * 3 + 4 * 5 + 6
1 + (2 * 3) + (4 * (5 + 6))
2 * 3 + (4 * 5)
5 + (8 * 3 + 9 + 3 * 4 * 3)
5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))
((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2"""

text = open("input.txt").read()

lines = text.split("\n")
while lines[-1] == "":
    lines = lines[:-1]


def calc(text):
    text = text.replace(" ", "")
    #print ("calc " + text)
    operator = ""
    firstTkn = True
    values = []
    while len(text) > 0:
        nextTkn = text[0]
        text = text[1:]
        if not firstTkn and operator == "":
            #print ("operator " + nextTkn)
            operator = nextTkn
        else:
            if (nextTkn=="("):
                nr = 1
                #print ("subtext:")
                subText = ""
                while len(text) > 0:
                    nextTkn = text[0]
                    text = text[1:]
                    if nextTkn[0] == "(":
                        nr += 1
                        #print (nr)
                    elif nextTkn[-1] == ")":
                        #print (nr)
                        nr -= 1
                        if nr == 0:
                            subText += nextTkn
                            value = calc(subText)
                            break
                    subText += nextTkn
                    #print(subText)
            else:
                #print ("value " + nextTkn)
                value = int(nextTkn)
            if (firstTkn):
                values.append( value)
            else:
                if (operator == "+"):
                    values[-1] += value
                elif (operator == "*"):
                    values.append( value)
                operator = ""
        firstTkn = False
    result = 1
    for v in values:
        result*=v
    if result < 0:
        print (text, values)
    return result

sum = int(0)
for l in lines:
    result = calc(l)
    #print (l + " = " + str(result))
    sum += result

print (sum)