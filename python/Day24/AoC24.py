text = """sesenwnenenewseeswwswswwnenewsewsw
neeenesenwnwwswnenewnwwsewnenwseswesw
seswneswswsenwwnwse
nwnwneseeswswnenewneswwnewseswneseene
swweswneswnenwsewnwneneseenw
eesenwseswswnenwswnwnwsewwnwsene
sewnenenenesenwsewnenwwwse
wenwwweseeeweswwwnwwe
wsweesenenewnwwnwsenewsenwwsesesenwne
neeswseenwwswnwswswnw
nenwswwsewswnenenewsenwsenwnesesenew
enewnwewneswsewnwswenweswnenwsenwsw
sweneswneswneneenwnewenewwneswswnese
swwesenesewenwneswnwwneseswwne
enesenwswwswneneswsenwnewswseenwsese
wnwnesenesenenwwnenwsewesewsesesew
nenewswnwewswnenesenwnesewesw
eneswnwswnwsenenwnwnwwseeswneewsenese
neswnwewnwnwseenwseesewsenwsweewe
wseweeenwnesenwwwswnew"""
text = open("input.txt").read()

lines = text.split("\n")
while lines[-1] == "":
    lines = lines[:-1]

tiles = {}
for l in lines:
    x=0
    y=0
    while len(l)>0:
        if l.startswith("e"):
            x+=1
            l = l[1:]
        elif l.startswith("se"):
            x += 1
            y -= 1
            l = l[2:]
        elif l.startswith("sw"):
            y-=1
            l = l[2:]
        elif l.startswith("w"):
            x-=1
            l = l[1:]
        elif l.startswith("ne"):
            y+=1
            l = l[2:]
        elif l.startswith("nw"):
            x-=1
            y+=1
            l = l[2:]
    if not (x,y) in tiles:
        tiles[(x,y)] = 0
    else:
        tiles[(x,y)] += 1

# 0 == black, 1==white
def get_colour(x,y,tiles):
    if not (x,y) in tiles:
        return 1
    else:
        return tiles[(x,y)] % 2


minx = min([x for (x,y) in tiles])
maxx = max([x for (x,y) in tiles])
miny = min([y for (x,y) in tiles])
maxy = max([y for (x,y) in tiles])

def count_blacks(tiles):
    nrblack = 0
    for (x,y) in tiles:
        if get_colour(x, y, tiles) == 0:
            nrblack +=1
    return nrblack

print (count_blacks(tiles))

dirs = [(0,1), (-1,1), (-1,0), (1,0), (0,-1), (1,-1)]
def count_black_neighbours(x,y,tiles):
    nr_black = 0
    for (dx,dy) in dirs:
        c = get_colour(x+dx, y+dy, tiles)
        #print ("({0}/{1}): {2}".format(x+dx, y+dy, c))
        if c == 0:
            nr_black += 1
    return nr_black

for day in range (1,101):
    minx -=1
    maxx +=1
    miny -=1
    maxy +=1
    #print (minx,maxx,miny,maxy)
    #print (tiles)
    newtiles = {}
    for x in range (minx, maxx+1):
        for y in range (miny, maxy+1):
            nrblack = count_black_neighbours(x,y,tiles)
            if (x,y) in tiles and tiles[(x,y)] == 0:
                if nrblack == 0 or nrblack>2:
                    newtiles[(x,y)] = 1
                else:
                    newtiles[(x,y)] = 0
            else:
                if nrblack == 2:
                    newtiles[(x, y)] = 0
                else:
                    newtiles[(x, y)] = 1
            #print ("{0}/{1}: {2} => {3}".format(x,y,nrblack, newtiles[(x, y)]))

    print ("Day {0}: {1}".format(day, count_blacks(newtiles)))
    tiles = newtiles
