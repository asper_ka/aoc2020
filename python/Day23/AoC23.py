cups = [int(x) for x in "389125467"]
#cups = [int(x) for x in "523764819"]
#print (cups)

maxnr = 9

for nr in range(10,maxnr):
    cups.append(nr)

def take_cups(cups, idx, nr):
    result = []
    if idx >= len(cups):
        idx = 0
    for i in range(nr):
        result.append(cups.pop(idx))
        if idx>=len(cups):
            idx=0
    return cups, result

ccup = 0
for move in range (100):
    cur_value = cups[ccup]
    print ("{0}: ccup: {1}({3}), {2}".format(move+1, ccup, cups, cur_value))
    nr = cups[ccup]
    cups, taken = take_cups(cups, ccup+1, 3)
    idx = -1
    nr -= 1
    while idx <0:
        if (nr == 0):
            nr = maxnr
        #print ("find "+str(nr))
        if nr in cups:
            idx = cups.index(nr)
        else:
            nr = nr-1
    print ("destination: {0}: insert {1} at pos {2}".format(nr, taken, idx))
    cups[idx+1:idx+1] = taken
    ccup = (cups.index(cur_value) + 1)%len(cups)
    idx = cups.index(1)
    result = str(idx)
    for i in range(2):
        result += " " +str(cups[(idx+1+i)%len(cups)])

    print (result)

#print (cups)
