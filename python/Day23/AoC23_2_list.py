cup_nrs = [int(x) for x in "389125467"]
cup_nrs = [int(x) for x in "523764819"]
#print (cups)

class cup:
    def __init__(self, nr):
        self.nr = nr
        self.next = None

nr_cups = len(cup_nrs)
first_cup = cup(cup_nrs.pop(0))
first_cup.next = first_cup
current_cup = first_cup

cups_by_nr = {}
cups_by_nr[first_cup.nr] = first_cup
while len(cup_nrs)>0:
    c = cup(cup_nrs.pop(0))
    cups_by_nr[c.nr] = c
    current_cup.next = c
    current_cup = c
for n in range (10, 1000001):
    c = cup(n)
    cups_by_nr[c.nr] = c
    current_cup.next = c
    current_cup = c
nr_cups = 1000000

current_cup.next = first_cup

def print_cups(c_cup):
    c = c_cup
    while True:
        print(c.nr, end=" ")
        c = c.next
        if not c or c == c_cup:
            break
    print()

current_cup = first_cup

def take_cups(c_cup):
    taken = c_cup.next
    end_cup = taken.next.next
    c_cup.next = end_cup.next
    end_cup.next = None
    return taken

def insert_cups(cup, cups):
    n = cup.next
    cup.next = cups
    cups.next.next.next = n

def nr_in_cups(cups, nr):
    c = cups
    while c:
        if nr == c.nr:
            return True
        c = c.next
    return False

def decr_number(nr):
    nr -= 1
    if nr==0:
        nr = nr_cups
    return nr

def move(move_nr, current_cup):
    #print ("-- move {0} --".format(move_nr))
    #print ("cups: ", end="")
    #print_cups(current_cup)
    taken = take_cups(current_cup)
    #print ("pick up: ", end="")
    #print_cups(taken)
    destination = decr_number(current_cup.nr)
    while nr_in_cups(taken, destination):
        destination = decr_number(destination)
    #print ("destination: {0}".format(destination))
    c = cups_by_nr[destination]
    insert_cups(c, taken)
    return current_cup.next

print ("start")
for m in range (10000000):
    current_cup = move(m, current_cup)
    if (m%10000) == 0:
        print (m)
while current_cup.nr!=1:
    current_cup = current_cup.next
print (current_cup.nr, current_cup.next.nr, current_cup.next.next.nr)
print (current_cup.next.nr * current_cup.next.next.nr)