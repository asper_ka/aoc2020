﻿using System;
using System.Collections.Generic;

namespace AoC2020
{
    public class FileReader
    {
        static public List<String> ReadFile (String path)
        {
            List<String> l_Lines = new List<String> ();
            System.IO.FileStream l_File = new System.IO.FileStream (path, System.IO.FileMode.Open, System.IO.FileAccess.Read);
            System.IO.StreamReader l_Stream = new System.IO.StreamReader (l_File);
            while (l_Stream.Peek () != -1)
            {
                l_Lines.Add (l_Stream.ReadLine ());
            }
            return l_Lines;
        }
        static public List<String> ReadFileAndSort (String path)
        {
            List<String> l_Lines = ReadFile (path);
            l_Lines.Sort ();
            return l_Lines;
        }
    }
}
